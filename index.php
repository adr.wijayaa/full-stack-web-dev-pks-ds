<?php

abstract class Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    use Fight;

    public function getInfoHewan();
    public function setJenis($nama, $jumlahKaki, $keahlian){
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
    }

    public function getJenis(){
        return $this->nama . ", berkaki " . $this->jumlahKaki . " memiliki keahlian " . $this->keahlian;
    }

    public function atraksi(){
        return $this->nama . " sedang " . $this->keahlian;
    }

}

trait Fight{
    public $attackPower;
    public $defensePower;

    public function get($attackPower, $defensePower){
        $this->attackPower = $attackPower;
        $this->defensePower = $defensePower;
    }

    public function serang($nama1, $nama2){
        return $this->nama1 . " sedang menyerang " . $this->nama2;
    }

    public function diserang($nama1, $nama2){
        return $this->nama1 . " sedang diserang " . $this->nama2;
    }

}

class Elang extends Hewan {
    public function getInfoHewan(){

    }
}

$elang = new Elang();
$elang->setJenis('Elang','4','Terbang Tinggi');
echo $elang->getJenis();